package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import static org.junit.Assert.*;

public class StorageTest {

    @Test
    public void getStockEntries_noEntries_shouldReturnEmptyCollection() {
        Storage storage = new Storage();
        assertTrue(storage.getStockEntries().isEmpty());
    }

    @Test
    public void getStockEntries_insertNewEntry_shouldReturnListWithOneEntry() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);

        assertEquals(1, storage.getStockEntries().size());

        Iterator<ItemStock> it = storage.getStockEntries().iterator();
        assertEquals(item.getID(), it.next().getItem().getID());
        assertFalse(it.hasNext());
    }

    @Test
    public void getStockEntries_insertSameEntry_shouldIncreaseCount() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);
        storage.insertItems(item, 1);

        assertEquals(1, storage.getStockEntries().size());

        Iterator<ItemStock> it = storage.getStockEntries().iterator();
        assertEquals(2, it.next().getCount());
    }

    @Test(expected = NoItemInStorage.class)
    public void removeItems_noItems_shouldThrowException() throws NoItemInStorage {
        Storage storage = new Storage();

        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);

        storage.removeItems(item, 10);
    }

    @Test(expected = NoItemInStorage.class)
    public void removeItems_insertOneItem_removeTwoItems_shouldThrowException() throws NoItemInStorage {
        Storage storage = new Storage();

        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);

        storage.insertItems(item, 1);

        storage.removeItems(item, 2);
    }

    @Test
    public void removeItems_insertItem_removeItem_shouldDecreaseCount() throws NoItemInStorage {
        Storage storage = new Storage();

        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);

        storage.insertItems(item, 1);
        storage.removeItems(item, 1);

        assertEquals(0, storage.getStockEntries().iterator().next().getCount());
    }

    @Test
    public void processOrder_emptyShoppingCard_shouldJustRun() throws NoItemInStorage {
        Order order = new Order(new ShoppingCart());
        new Storage().processOrder(order);
    }

    @Test(expected = NoItemInStorage.class)
    public void processOrder_itemNotInStorage_shouldThrowException() throws NoItemInStorage {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "apple", 1f, "food", 15));
        Order order = new Order(cart);
        new Storage().processOrder(order);
    }

    @Test
    public void getItemCount_noSuchItem_shouldReturnZero() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);

        StandardItem unknownItem = new StandardItem(10, "apple", 1f, "food", 15);

        assertEquals(0, storage.getItemCount(unknownItem));
    }

    @Test
    public void getItemCount_itemFound_shouldReturnNumberOfInsertedItems() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 15);

        assertEquals(15, storage.getItemCount(item));
    }

    @Test
    public void getItemCount_byId_noSuchItem_shouldReturnZero() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 1);

        StandardItem unknownItem = new StandardItem(10, "apple", 1f, "food", 15);

        assertEquals(0, storage.getItemCount(unknownItem.getID()));
    }

    @Test
    public void getItemCount_byId_itemFound_shouldReturnNumberOfInsertedItems() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 1f, "food", 15);
        storage.insertItems(item, 15);

        assertEquals(15, storage.getItemCount(item.getID()));
    }

    @Test
    public void getPriceOfWholeStock_emptyStack_shouldReturnZero() {
        Storage storage = new Storage();

        assertEquals(0, storage.getPriceOfWholeStock());
    }

    @Test
    public void getPriceOfWholeStock_nonEmptyStack_shouldReturnSumOfPrices() {
        Storage storage = new Storage();
        StandardItem item = new StandardItem(1, "apple", 55F, "food", 15);

        storage.insertItems(item, 1);

        assertEquals(55F, storage.getPriceOfWholeStock(), 0);
    }

    @Test
    public void getItemsOfCategorySortedByPrice_unknownCategory_shouldReturnEmptyColection() {
        Storage storage = new Storage();
        assertTrue(storage.getItemsOfCategorySortedByPrice("unknown").isEmpty());

    }

    @Test
    public void sortItemsByPriceTest() {
        //setup
        Storage storage = new Storage();
        //sorted list
        ArrayList<Item> list1 = new ArrayList<Item>();
        Item item1 = new StandardItem(1, "apple", 25f, "food", 15);
        Item item2 = new StandardItem(2, "orange", 30f, "food", 20);
        Item item3 = new StandardItem(3, "banana", 32f, "food", 10);
        list1.add(item1);
        list1.add(item2);
        list1.add(item3);
        //the same list, but not sorted
        ArrayList<Item> list2 = new ArrayList<Item>();
        list2.add(item1);
        list2.add(item3);
        list2.add(item2);

        //ast
        storage.sortItemsByPrice(list2);

        //assert
        assertEquals(list1, list2);
    }

    @Test
    public void sortItemsByPriceTestEmptyList() {
        //setup
        Storage storage = new Storage();
        //empty list
        ArrayList<Item> list = new ArrayList<Item>();
        //act
        storage.sortItemsByPrice(list);

        //assert
        assertEquals(list, list);
    }

    @Test
    //test for nullPointerExeption
    public void sortItemsByPriceTestNull() {
        //setup
        Storage storage = new Storage();

        //act
        storage.sortItemsByPrice(null);
    }

    @Test
    public void constructorTest() {
        //setup + act
        Storage storage = new Storage();
        //assert
        assertNotNull(storage);
    }

    @Test
    public void parametrizedConstructorTest() {
        //setup + act
        StandardItem item = new StandardItem(1, "name", 3.14f, "category", 10);
        ItemStock itemStock = new ItemStock(item);
        HashMap<Integer, ItemStock> stock = new HashMap<Integer, ItemStock>();
        stock.put(1, itemStock);
        Storage storage = new Storage(stock);

        assertNotNull(storage);
        assertEquals(itemStock, storage.getStockEntries().iterator().next());

    }

    @Test
    public void getItemsByCategoryTest() {
        //setup
        Item item1 = new StandardItem(1, "apple", 25f, "food", 15);
        ItemStock itemStock1 = new ItemStock(item1);
        Item item2 = new StandardItem(2, "orange", 30f, "food", 20);
        ItemStock itemStock2 = new ItemStock(item2);
        Item item3 = new StandardItem(3, "banana", 32f, "food", 10);
        ItemStock itemStock3 = new ItemStock(item3);
        Item item4 = new StandardItem(4, "chair", 130f, "furniture", 123);
        ItemStock itemStock4 = new ItemStock(item4);
        HashMap<Integer, ItemStock> stock = new HashMap<Integer, ItemStock>();
        stock.put(1, itemStock1);
        stock.put(2, itemStock4);
        stock.put(3, itemStock3);
        stock.put(4, itemStock2);
        Storage storage = new Storage(stock);

        //act
        ArrayList<Item> actualList = storage.getItemsByCategory("food");

        //creation of expectedList
        ArrayList<Item> expectedList = new ArrayList<Item>();

        expectedList.add(item1);
        expectedList.add(item3);
        expectedList.add(item2);

        //assert
        assertEquals(expectedList, actualList);
    }
}