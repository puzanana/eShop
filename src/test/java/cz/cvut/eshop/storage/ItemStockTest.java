package cz.cvut.eshop.storage;

import org.junit.Test;
import cz.cvut.eshop.shop.StandardItem;

import static org.junit.Assert.assertEquals;

public class ItemStockTest {

    @Test
    public void increaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.IncreaseItemCount(10);

        assertEquals(10, itemStock.getCount());
    }

    @Test
    public void decreaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.decreaseItemCount(10);

        assertEquals(-10, itemStock.getCount());
    }

    @Test
    public void toStringTest() {
        // setup
        StandardItem item = new StandardItem(1, "name", 3.14f, "category", 10);
        ItemStock itemStock = new ItemStock(item);

        //act
        String expected = "STOCK OF ITEM:  Item   ID 1   NAME name   CATEGORY category   PRICE 3.14   LOYALTY POINTS 10    PIECES IN STORAGE: 0";

        //assert
        assertEquals(expected, itemStock.toString());
    }

    @Test
    public void toStringTestNull() {
        //setup
        ItemStock itemStock = new ItemStock(null);

        assertEquals(null, itemStock.toString());
    }

}