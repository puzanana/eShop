package cz.cvut.eshop.shop;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class OrderTest {

    ShoppingCart shoppingCart = new ShoppingCart();
    Order order;
    Customer customer = new Customer(0);
    @Before
    public void fillShoppingCartAndCreateOrderAndAddCustomer () {
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        Item item2 = new StandardItem(2, "apple", 2.50f, "food", 5);
        shoppingCart.addItem(item);
        shoppingCart.addItem(item2);

        order = new Order(shoppingCart);

        order.setCustomer(customer);
    }

    @Test
    public void create_orderCreatedFromShoppingCardBefore_shouldSetLoyaltyPointToCustomer() {

        order.create();

        assertEquals(8, customer.getLoyaltyPoints());
    }

    @Test
    public void applyDiscount_usingSC() {
        order.create();
        order.applyDiscount();

        float discount = (float) (0.2 * 8);
        float price = 7 - discount;

        assertEquals(price, order.getTotalAmount(), 0.05f);

    }

    @Test
    public void getTotalAmount_usingSameSC_shouldBe7() {
        assertEquals(7f, order.getTotalAmount(), 0.05f);
    }

    @Test
    public void parametrizedConstructorTest() {
        //setup + act
        ShoppingCart shoppingCart1 = new ShoppingCart();
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        Item item2 = new StandardItem(2, "apple", 2.50f, "food", 5);
        shoppingCart1.addItem(item);
        shoppingCart1.addItem(item2);
        Date purchaseOrder = new Date();

        Order order1 = new Order(shoppingCart1, purchaseOrder);

        //assert
        assertNotNull(order1);
        assertEquals(0, order1.getState());
    }
}